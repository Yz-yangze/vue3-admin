import axios from '@/utils/axios'

// 登录接口
export const login = (data) => {
  return axios({
    url: '/sys/login',
    method: 'POST',
    data
  })
}

// 请求用户信息接口
export const getUserInfo = () => {
  return axios.get('/sys/profile')
}
