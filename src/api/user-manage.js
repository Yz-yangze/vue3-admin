import axios from '@/utils/axios'

/**
 * 用户列表分页展示
 */
export const getUserManageList = (params) => {
  return axios({
    url: '/user-manage/list',
    params
  })
}

/**
 * 批量导入
 */
export const userBatchImport = (data) => {
  return axios({
    url: '/user-manage/batch/import',
    method: 'POST',
    data
  })
}

/** * 删除指定数据 */
export const deleteUser = (id) => {
  return axios({ url: `/user-manage/detele/${id}` })
}
