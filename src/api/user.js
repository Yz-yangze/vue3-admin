import axios from '@/utils/axios'

export const getFeature = () => {
  return axios({
    url: '/user/feature'
  })
}

/**
 * 获取章节目录
 */

export const getChapter = () => {
  return axios({
    url: '/user/chapter'
  })
}
