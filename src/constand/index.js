/**
 * 这是一个用来专门放常量的文件夹
 */

// 要储存的变量名(时间戳)
export const TIME_STAMP = 'timeStamp'

// 规定的时常,多长时间算过期
export const TIME_OUT = 2 * 3600 * 1000

// 国际化
export const LANG = 'language'

// 储存主题颜色的变量名
export const MAIN_COLOR = 'mainColor'

// 默认的颜色值
export const DEFAULT_COLOR = '#409eff'

// tagViews
export const TAG_VIEWS = 'tagviews'
