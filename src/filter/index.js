import dayjs from 'dayjs'

const dateFilter = (val, format = 'YYYY-MM-DD') => {
  // 判断能不能转化为数字
  if (!isNaN(val)) {
    val = parseInt(val)
  }
  // 通过这个插件处理时间
  return dayjs(val).format(format)
}
// 导出方法
export default (app) => {
  app.config.globalProperties.$filters = {
    dateFilter
  }
}
