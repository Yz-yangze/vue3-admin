// 导入index.css
import 'element-plus/dist/index.css'
import {
  ElButton,
  ElForm,
  ElFormItem,
  ElInput,
  ElIcon,
  ElRow,
  ElMessage,
  ElDropdown,
  ElDropdownItem,
  ElDropdownMenu,
  ElAvatar,
  ElMenu,
  ElMenuItem,
  ElMenuItemGroup,
  ElSubMenu,
  ElScrollbar,
  ElBreadcrumb,
  ElBreadcrumbItem,
  ElTooltip,
  ElConfigProvider,
  ElPagination,
  ElDialog,
  ElColorPicker,
  ElSelect,
  ElOption,
  ElCol,
  ElTabPane,
  ElTabs,
  ElCard,
  ElProgress,
  ElCollapse,
  ElCollapseItem,
  ElTimeline,
  ElTimelineItem,
  ElTable,
  ElTableColumn,
  ElImage,
  ElTag,
  ElMessageBox
} from 'element-plus/lib/components' // 需要的就导入

// 使用icon必须从这个里面导入
import { Tools, User } from '@element-plus/icons' // 使用icon必须引入

const components = [
  // 需要的就导入
  ElButton,
  ElCol,
  ElTag,
  ElOption,
  ElSelect,
  ElConfigProvider,
  ElPagination,
  ElDialog,
  ElColorPicker,
  ElTooltip,
  ElForm,
  ElFormItem,
  ElInput,
  ElIcon,
  ElRow,
  ElMessage,
  ElMessageBox,
  User,
  ElDropdown,
  ElDropdownItem,
  ElDropdownMenu,
  ElAvatar,
  Tools,
  ElMenu,
  ElMenuItem,
  ElMenuItemGroup,
  ElSubMenu,
  ElScrollbar,
  ElBreadcrumb,
  ElBreadcrumbItem,
  ElTabPane,
  ElTabs,
  ElCard,
  ElProgress,
  ElCollapse,
  ElCollapseItem,
  ElTimeline,
  ElTimelineItem,
  ElTable,
  ElTableColumn,
  ElImage
]

export default function (app) {
  for (const component of components) {
    app.component(component.name, component)
  }
}
