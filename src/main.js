import { createApp } from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import { globalRegister } from './global'
import './style/index.scss'
// 导入 svgIcon
import installIcons from '@/icons'
import './permission' // 登录鉴权模块
// i18n 国际化
import i18n from '@/i18n'
// 处理时间
import installFilter from '@/filter'

const app = createApp(App)
installIcons(app)
installFilter(app)
app.use(globalRegister).use(i18n).use(store).use(router).mount('#app')
