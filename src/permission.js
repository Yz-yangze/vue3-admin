import router from '@/router'
import store from './store'
import { getItem } from './utils/storage'

// 白名单
var whiteList = ['/login']

/**
 * 登 录鉴权模块,  路由前置守卫
 */
router.beforeEach(async (to, from, next) => {
  // 首先判断是否有token,来确定是不是登录状态
  if (getItem('token')) {
    // 登陆状态下不能去login页面
    if (to.path === '/login') {
      next('/')
    } else {
      // 是登录状态的话,就判断现在有没有登录信息,有的话就啥也不干,没有的话就发送请求来获取用户信息
      if (store.getters.userInfo) {
        // 在这发送请求
        await store.dispatch('user/getUserInfo')
      }
      next()
    }
  } else {
    // 没有登录的话，只能去login页面
    if (whiteList.indexOf(to.path) > -1) {
      next()
    } else {
      next('/login')
    }
  }
})
