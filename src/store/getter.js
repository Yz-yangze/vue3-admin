import store from '@/store'
import { generateColors } from '@/utils/theme.js'
import { getItem } from '@/utils/storage'
import { MAIN_COLOR } from '../constand/index'

const getters = {
  // 用来判断是否有用户信息
  userInfo: () => {
    return JSON.stringify(store.state.user.userInfo) === '{}'
  },
  // 拿到用户信息
  userData: () => {
    return store.state.user.userInfo
  },
  // 拿到定义的整体的css样式
  cssVar: (state) => {
    // 这里还需要把最新的色值表给加进去
    return {
      ...state.theme.variables,
      ...generateColors(getItem(MAIN_COLOR))
    }
  },
  // 左侧菜单栏是否伸缩的flag
  sidebarOpened: () => {
    return store.state.app.sliderBarOpend
  },
  // 获取国际化是英文还是中文
  language: () => {
    return store.state.app.language
  },
  // 获取主题色
  themeColor: () => {
    return store.state.theme.mainColor
  },
  // 获取tagsviews的数据
  tagsViewList: () => {
    return store.state.app.tagViewsList
  }
}

export default getters
