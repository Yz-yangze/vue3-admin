import { LANG, TAG_VIEWS } from '@/constand'
import { getItem, setItem } from '@/utils/storage'

export default {
  namespaced: true,
  state: () => ({
    sliderBarOpend: true, // 是否伸缩
    language: getItem(LANG) || 'zh',
    tagViewsList: getItem(TAG_VIEWS) || []
  }),
  mutations: {
    // 每次点击的时候都取反
    setOpend: (state) => {
      state.sliderBarOpend = !state.sliderBarOpend
    },
    // 设置国际化
    setLanguage: (state, payload) => {
      state.language = payload
      setItem(LANG, payload)
    },
    // tagviews
    setTagViewsList: (state, payload) => {
      // 要做去重处理
      const isShow = state.tagViewsList.find((item) => {
        return item.path === payload.path
      })
      // 添加进去
      if (!isShow) state.tagViewsList.push(payload)
      // 储存起来
      setItem(TAG_VIEWS, state.tagViewsList)
    },
    /**
     * 为指定的 tag 修改 title
     */
    changeTagsView(state, { index, tag }) {
      state.tagViewsList[index] = tag
      setItem(TAG_VIEWS, state.tagViewsList)
    },
    /**
     * @param {index || right || other} payload
     */
    removeTagsView(state, payload) {
      if (payload.type === 'index') {
        // 关闭自己
        state.tagViewsList.splice(payload.index, 1)
      } else if (payload.type === 'right') {
        // 关闭右侧
        state.tagViewsList.splice(
          payload.index + 1,
          state.tagViewsList.length - payload.index + 1
        )
      } else if (payload.type === 'other') {
        // 关闭其他
        state.tagViewsList.splice(
          payload.index + 1,
          state.tagViewsList.length - payload.index + 1
        )
        state.tagViewsList.splice(0, payload.index)
      }
      setItem(TAG_VIEWS, state.tagViewsList)
    }
  }
}
