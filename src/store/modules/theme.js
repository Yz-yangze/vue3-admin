import { MAIN_COLOR, DEFAULT_COLOR } from '@/constand'
import { getItem, setItem } from '@/utils/storage'
import variables from '@/style/variables.scss'

export default {
  namespaced: true,
  state: () => ({
    mainColor: getItem(MAIN_COLOR) || DEFAULT_COLOR,
    variables: variables
  }),
  mutations: {
    // 在两个地方都把主题色保存起来
    setMainColor: (state, payload) => {
      state.mainColor = payload
      state.variables.menuBg = payload
      setItem(MAIN_COLOR, payload)
    }
  }
}
