import { login, getUserInfo } from '@/api/login'
import md5 from 'md5'
import { setItem, getItem, removeAllItem } from '@/utils/storage'
import router from '@/router'
import { setTimeStamp } from '@/utils/authTime'

export default {
  namespaced: true,
  state: () => ({
    token: getItem('token') || '',
    userInfo: {}
  }),
  mutations: {
    // 储存token
    setToken(state, token) {
      // 1 放到vuex中
      state.token = token
      // 2 本地存储
      setItem('token', token)
    },
    // 储存用户信息
    setUserInfo(state, userInfo) {
      state.userInfo = userInfo
    }
  },
  actions: {
    // 登录动作
    login({ commit }, data) {
      const { username, password } = data
      // 返回一个promise可以做到一个捕获
      return new Promise((resolve, reject) => {
        // 这是登录的请求接口
        login({
          username,
          password: md5(password)
        }) // 成功的时候
          .then((res) => {
            // 需要储存token到本地存和vuex中都需要
            this.commit('user/setToken', res.data.token)
            // 登录成功的话跳转到首页
            router.push('/')
            // 登录成功了还需要再缓存一个时间戳
            setTimeStamp() // 用于判断登陆状态是否过期
            resolve()
          })
          // 失败的时候
          .catch((err) => {
            reject(err)
          })
      })
    },
    // 获取用户信息动作
    async getUserInfo({ commit }) {
      const res = await getUserInfo()
      commit('setUserInfo', res)
    },
    // 退出登录
    logout({ commit }) {
      // 退出登录需要把本地存储的东西全部删除,vuex里面保存的token也得删除
      commit('setToken', '')
      commit('setUserInfo', {})
      removeAllItem()
      router.push('/login')
    }
  }
}
