/**
 * 这是一个专门计算登陆时间是否过期的文件
 */

import { getItem, setItem } from './storage'
import { TIME_OUT, TIME_STAMP } from '../constand'

// 获取时间戳
export const getTimeStamp = () => {
  return getItem(TIME_STAMP)
}

// 设置时间戳
export const setTimeStamp = () => {
  setItem(TIME_STAMP, Date.now())
}

// 检查是否过期
export const isCheckOut = () => {
  // 这是登录时候的时间戳
  const LoginTime = getTimeStamp(TIME_STAMP)
  // 再拿到现在的时间戳
  const currentTime = Date.now()
  // 判断两个时间戳之间的差距是否大于定义的时间戳
  return currentTime - LoginTime > TIME_OUT
}
