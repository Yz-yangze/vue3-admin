import axios from 'axios'
import { ElMessage } from 'element-plus/lib/components'
import { getItem } from '@/utils/storage'
import { isCheckOut } from '@/utils/authTime'
import store from '@/store'

const httpAxios = axios.create({
  baseURL: process.env.VUE_APP_BASE_URL,
  timeout: 5000
})

// 添加请求拦截器
httpAxios.interceptors.request.use(
  function (config) {
    // 请求之前需要判断一下登录状态是否过期,过期的话就跳到登录页面
    if (isCheckOut() && getItem('token')) {
      // 过期状态的话,要被动退出登录,要走vuex里的退出登录
      store.dispatch('user/logout')
      return Promise.reject(new Error('token过期,请重新登录'))
    }
    // 在发送请求之前做些什么
    return {
      ...config,
      headers: {
        Authorization: `Bearer ${getItem('token')}` || '',
        icode: '740EB9997B129AC7',
        'Accept-Language': store.getters.language
      }
    }
  },
  function (error) {
    // 对请求错误做些什么
    return Promise.reject(error)
  }
)

// 添加响应拦截器
httpAxios.interceptors.response.use(
  (response) => {
    const { message, success } = response.data
    // 请求成功的时候,业务也成功的时候
    if (success) {
      return response.data
    } else {
      // 请求成功,业务失败的时候，比如404，500这些
      ElMessage.error(message)
      return Promise.reject(new Error(message))
    }
  },
  function (error) {
    if (error?.response?.data?.code === 401) {
      store.dispatch('user/logout')
    }
    // 请求就失败的时候
    ElMessage.error(error.message)
    return Promise.reject(error)
  }
)

export default httpAxios
