import i18n from '@/i18n'
import { watch } from 'vue'
import { useStore } from 'vuex'

// 在非组件的时候要用i18n的话必须使用这种方式
export function generateTitle(title) {
  return i18n.global.t('msg.route.' + title)
}

/**
 * @param  {...any} cbs 所有的回调
 */
export function watchSwitchLang(...cbs) {
  const store = useStore()
  watch(
    () => store.getters.language,
    () => {
      cbs.forEach((cb) => cb(store.getters.language))
    }
  )
}
