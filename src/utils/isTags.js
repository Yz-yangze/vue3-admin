const whiteList = ['/login', '/404', '/401']
/**
 * @param {String} path
 * @returns {Boolean}
 */
export const isTags = ({ path }) => {
  return whiteList.includes(path)
}
