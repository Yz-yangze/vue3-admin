import path from 'path'
/**
 * 这里生成页面menu菜单需要得数据
 */

// 这个方法用来先拿到所有有children的路由里面的所有children
const getChildrenRoutes = (routes) => {
  const result = []
  routes.forEach((item) => {
    if (item.children && item.children.length > 0) {
      result.push(...item.children)
    }
  })
  return result
}

/**
 * 判断一个数据是不是为空,或者空数组空对象
 */
const isNull = (data) => {
  if (!data) return true
  if (JSON.stringify(data) === '{}') return true
  if (JSON.stringify(data) === '[]') return true
}

/**
 * 因为拿到得是全部得路由数据,所以需要剔除一些数据
 * 有些路由已经在某些路由下面存在,所以就需要把这些路由剔除出去
 * 保留得就是有二级路由得
 */
export const filterRoutes = (routes) => {
  const childrenRoutes = getChildrenRoutes(routes) // 所有的子路由
  /**
   * 通过循环判断,所有的路由循环查找,如果里面有和子路由一样的,那么就不要这个路由
   */
  return routes.filter((item) => {
    return !childrenRoutes.find((ite) => {
      return item.path === ite.path
    })
  })
}

/**
 * @param {*} routes 这个routes代表的是经过过滤以后的routes,已经没有重复的了
 * @param {*} basePath 这个是用来拼接路径的
 * @returns {Arrar} 返回的是生成左侧菜单的数组
 */
export const generateMenus = (routes, basePath = '') => {
  const result = []
  // 遍历路由表
  routes.forEach((item) => {
    // 不存在 children && 不存在 meta 直接 return
    if (isNull(item.meta) && isNull(item.children)) return
    // 存在 children 不存在 meta,进入迭代
    if (isNull(item.meta) && !isNull(item.children)) {
      result.push(...generateMenus(item.children))
      return
    }
    // 合并 path 作为跳转路径
    const routePath = path.resolve(basePath, item.path)
    // 路由分离之后，存在同名父路由的情况，需要单独处理
    let route = result.find((item) => item.path === routePath)
    if (!route) {
      route = {
        ...item,
        path: routePath,
        children: []
      }

      // icon 与 title 必须全部存在
      if (route.meta.icon && route.meta.title) {
        // meta 存在生成 route 对象，放入 arr
        result.push(route)
      }
    }

    // 存在 children 进入迭代到children
    if (item.children) {
      route.children.push(...generateMenus(item.children, route.path))
    }
  })
  return result
}
