// 这个文件用来专门处理本地存储

/*
 * 存数据
 */
export const setItem = (key, value) => {
  // 判断是基本类型还是复杂数据类型,复杂类型的话需要json格式转化
  if (typeof value === 'object') {
    value = JSON.stringify(value)
  }
  window.localStorage.setItem(key, value)
}

/*
 * 拿数据
 */
export const getItem = (key) => {
  // 拿数据的时候时候,有的需要parse转换,有的不需要转化，也需要做一个容错处理
  const data = localStorage.getItem(key)
  try {
    return JSON.parse(data)
  } catch (error) {
    return data
  }
}

/*
 * 删除单一数据
 */
export const removeItem = (key) => {
  localStorage.removeItem(key)
}

/*
 * 删除全部数据
 */
export const removeAllItem = () => {
  localStorage.clear()
}
