import rgbHex from 'rgb-hex' // 可以把rgba格式的颜色转化成十六进制的颜色
import color from 'css-color-function' // 可以把css写成函数类的格式,可以解析成rgba格式
import axios from 'axios' // 这里是用来请求css,不用封装好的ajax
import formula from '@/constand/formula.json' // 色值表
/**
 *  首先要改变element-plus的主题颜色的话分为三步
 *  1. 获取当前style的所有值
 *  2. 利用正则替换掉要被更改的值
 *  3. 把修改过的样式重新再加载一次
 */

/**
 * 写入新样式到 style
 * @param {*} elNewStyle  element-plus 的新样式
 * @param {*} isNewStyleTag 是否生成新的 style 标签
 */
export const writeNewStyle = (elNewStyle) => {
  const style = document.createElement('style')
  style.innerText = elNewStyle
  document.head.appendChild(style)
}

/**
 * @description 根据选好的颜色生成样式表
 * @param {*} primaryColor 主色值,也就是颜色选择器选到的值
 * @returns 返回新的style样式,利用样式优先级替换到原来的样式
 */
export const generateNewStyle = async (primaryColor) => {
  // 拿到根据选好的颜色生成的全新样式表
  const colors = generateColors(primaryColor)
  // 拿到element-plus的默认样式表
  var cssText = await getOriginalStyle()
  // 遍历生成的样式表，在 CSS 的原样式中进行全局替换
  Object.keys(colors).forEach((key) => {
    cssText = cssText.replace(
      // 把被标记过的样式表中的标记都替换成根据选择的颜色生成的最新的色值表
      new RegExp('(:|\\s+)' + key, 'g'),
      '$1' + colors[key]
    )
  })

  return cssText
}

// 根据主色生成色值表
export const generateColors = (primary) => {
  if (!primary) return
  const colors = {
    primary
  }
  // 循环写好的json对象
  Object.keys(formula).forEach((item) => {
    // 这步就是把之前定义好的色值里面的主色PRIMARY这个替换成咱们选好的主题色
    const value = formula[item].replace(/primary/g, primary)
    // 再生成一份新的色值表
    colors[item] = '#' + rgbHex(color.convert(value))
  })

  return colors
}

/**
 * 拿到element-plus的默认样式表,被标记过的
 */
const getOriginalStyle = async () => {
  // 拿到当前的版本
  const version = require('element-plus/package.json').version
  // element已经把样式表放到一个网站上,可以请求回来
  const { data } = await axios.get(
    `https://unpkg.com/element-plus@${version}/dist/index.css`
  )
  // 拿到的数据还要做一些标记,需要把一些固定的样式标记出来好进行替换
  return getStyleTemplate(data)
}

/**
 * 这个方法是把element的样式中要被改的做一个标记
 * @param {*} 这个参数是本身的全部样式表
 */
const getStyleTemplate = (data) => {
  // element-plus 默认色值,就要把这些默认色值标记成后面的
  const colorMap = {
    '#3a8ee6': 'shade-1',
    '#409eff': 'primary',
    '#53a8ff': 'light-1',
    '#66b1ff': 'light-2',
    '#79bbff': 'light-3',
    '#8cc5ff': 'light-4',
    '#a0cfff': 'light-5',
    '#b3d8ff': 'light-6',
    '#c6e2ff': 'light-7',
    '#d9ecff': 'light-8',
    '#ecf5ff': 'light-9'
  }
  // 根据默认色值为要替换的色值打上标记
  Object.keys(colorMap).forEach((key) => {
    const value = colorMap[key]
    data = data.replace(new RegExp(key, 'ig'), value)
  })
  return data
}
