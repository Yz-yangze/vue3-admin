/**
 * 判断是否为外部资源
 */
export function isExternal(path) {
  // 以这些为开头的肯定就是外部资源传进来的
  return /^(https?:|mailto:|tel:)/.test(path)
}
